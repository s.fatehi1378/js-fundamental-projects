export class UI {

    addBookToList(book) {
        const list = document.getElementById("book-list")
        const row = document.createElement("tr");
        row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a class ="delete" href="#">X</a></td>`
        list.appendChild(row)
    }

    deleteBook(target) {
        if (target.className = "delete") {
            target.parentElement.parentElement.remove();
        }
    }

    showAlert(message , className) {
        const div = document.createElement("div");
        div.className =`alert ${className}`;
        div.appendChild(document.createTextNode(message));

        const container = document.querySelector('.container');
        const form = document.querySelector('#book-form')
        container.insertBefore(div , form);

        setTimeout(()=>{
            document.querySelector('.alert').remove();
        },3000);
    }


    clearFields(documentUI, authorUI, isbnUI) {
        documentUI.value ="";
        authorUI.value ="";
        isbnUI.value ="";
        
    }
}