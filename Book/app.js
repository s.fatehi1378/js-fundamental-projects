import { UI } from './ui.js';
import { Book } from './Book.js';
import { Store } from './store.js';

const submitBtn = document.querySelector('#submit-btn')
const ui = new UI();

  // DOM Load Event
  document.addEventListener('DOMContentLoaded', Store.displayBooks(ui));

submitBtn.addEventListener('click' , ()=>{
    const title = document.querySelector('#title');
    const author = document.querySelector('#author');
    const isbn = document.querySelector('#isbn');
    if(title.value === '' || author.value === '' || isbn.value === '')
    {
        ui.showAlert('plz fill in all fields' , 'error');
    }else{
        const book = new Book(title.value , author.value , isbn.value);
        Store.addBook(book)
        ui.addBookToList(book);
        ui.showAlert('Book added.', 'success');
        ui.clearFields(title , author , isbn);

    }
})


document.getElementById('book-list').addEventListener('click', (e)=>{
    ui.deleteBook(e.target);
    console.log(e.target.parentElement.previousElementSibling.textContent);
    if(e.target.parentElement.previousElementSibling.textContent !== null)
    Store.removeBook(e.target.parentElement.previousElementSibling.textContent);
    ui.showAlert('Book Removed!', 'success');
    e.preventDefault();
  });


