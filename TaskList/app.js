const form= document.querySelector('#task-form');
const taskList = document.querySelector('.collection');
const clearBtn = document.querySelector('.clear-tasks');
const filter = document.querySelector('#filter');
const taskInput = document.querySelector('#task');


setEventListeners()

function setEventListeners(){
    form.addEventListener('submit' , addTask);
    clearBtn.addEventListener('click' , deleteAllTasks);
    filter.addEventListener('keyup', filterTasks);
}


function addTask(e){
    if(taskInput.valuse === ''){
        alert('add a task');
    }

    const li = document.createElement('li');
    li.className = 'collection-item';
    li.appendChild(document.createTextNode(taskInput.value));

    const link = document.createElement('a');
    link.className = 'delete-item secondary-content';
    link.innerHTML ='<i class="fa fa-remove"></i>';
    link.addEventListener('click' , deleteTask)
    li.appendChild(link);

    taskList.appendChild(li);
    taskInput.value='';
    e.preventDefault();


}

function deleteTask(e){
    e.target.parentElement.parentElement.remove();
}

function deleteAllTasks(){
  //  taskList.innerHTML=''

    while(taskList.firstChild){
        taskList.remove(taskList.firstChild)
    }
}

// Filter Tasks
function filterTasks(e) {
    const text = e.target.value.toLowerCase();
    console.log('');
  
    document.querySelectorAll('.collection-item').forEach(function(task){
      const item = task.firstChild.textContent;
      if(item.toLowerCase().indexOf(text) != -1){
        task.style.display = 'block';
      } else {
        task.style.display = 'none';
      }
    });
}