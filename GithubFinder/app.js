import { Git } from "./git.js";
import { UI } from "./ui.js";

const git = new Git();
const ui = new UI();
const searchUser = document.getElementById
    ('searchUser');
searchUser.addEventListener('keyup', (e) => {
    const userText = e.target.value;

    if (userText !== '') {
        git.getUser(userText)
            .then(data => {
                if (data.profile.message === 'Not Found') {
                    ui.showAlert('User not found', 'alert alert-danger');
                }
                else {
                    console.log(data.repos);
                    ui.getProfileUI(data.profile);
                    ui.getReposUI(data.repos);
                }
            })
    } else {
        ui.clearProfile();
    }
});